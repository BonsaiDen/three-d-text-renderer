uniform mat4 viewProjection;
uniform mat4 modelMatrix;

in vec3 position;
in vec4 color;
in vec2 quad_coordinates;
in vec2 glyph_coordinates;

out vec3 pos;
out vec2 quad_uvs;
out vec2 glyph_uvs;
out vec4 glyph_color;

void main() {
    mat4 local2World = modelMatrix;
    vec4 worldPosition = local2World * vec4(position, 1.);
    worldPosition /= worldPosition.w;
    gl_Position = viewProjection * worldPosition;
    pos = worldPosition.xyz;
    glyph_color = color;
    glyph_uvs = glyph_coordinates;
    quad_uvs = quad_coordinates;
}

