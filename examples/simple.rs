use three_d::*;
use three_d_asset::io::load;
use three_d_text_builder::{TextRef, TextBuilder, TextBuilderSettings, TextAlign, TextPosition, Text};

pub fn main() {
    let window = Window::new(WindowSettings {
        title: "Text Rendering".to_string(),
        max_size: Some((1280, 720)),
        ..Default::default()
    }).unwrap();
    let context = window.gl();

    // Load our desired ttf font
    let assets = load(&["examples/ubuntu.mono.ttf"]).unwrap();

    // Create a text builder for the given font with default settings
    let mut text_builder = TextBuilder::new(
        assets.get("ubuntu.mono.ttf").unwrap(),
        TextBuilderSettings::default()

    ).expect("Failed to create text builder from TTF font");

    // Create a "2d" camera for our text to be rendered with
    let mut text_camera = Camera::new_orthographic(
        Viewport::new_at_origo(1, 1),
        vec3(0.0, 0.0, 0.0),
        vec3(0.0, 0.0, -1.0),
        vec3(0.0, 1.0, 0.0),
        0.0,
        0.1,
        10.0,
    );

    let text = Text {
        // The text to render
        text: "The quick brown fox jumps over the lazy dog".to_string(),
        // Align to the lower center edge of the viewport
        align: TextAlign::Viewport(0, -1),
        // Add some padding
        padding: vec2(0.0, 8.0),
        // Move up by 25% of the viewport's height
        position: TextPosition::Percentage(vec2(0.0, 0.25)),
        // Add a simple shadow effect
        shadow: Some((Srgba::BLACK, vec2(1.0, -1.0))),
        ..Default::default()
    };

    window.render_loop(move |mut frame_input| {
        // Ensure the viewport size is a multiple of two (to prevent character
        // mis-alignment caused by the camera projection) and adjust it to be
        // a 1:1 pixel match with the window
        frame_input.viewport.width &= !0b1;
        frame_input.viewport.height &= !0b1;
        text_camera.set_viewport(frame_input.viewport);
        text_camera.set_orthographic_projection(frame_input.viewport.height as f32, 0.0, 1.0);

        // Set the text builder's viewport so that TextAlign::Viewport work correctly
        text_builder.set_viewport(frame_input.viewport);

        // Render text
        let text_model = text_builder.build(&context, &[
            TextRef {
                text: &format!("Seconds: {:.2}", frame_input.accumulated_time / 1000.0),
                align: TextAlign::ScreenRelative(0, 0),
                ..Default::default()
            },
            text.as_ref()
        ]);

        let target = frame_input.screen();
        target.clear(ClearState::color_and_depth(0.1, 0.2, 0.3, 1.0, 1.0));
        target.render(&text_camera, text_model, &[]);

        FrameOutput::default()
    });
}

