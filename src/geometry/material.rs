// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use three_d::{Blend, Camera, Context, Program, RenderStates, Texture2D, WriteMask, ColorMapping, ToneMapping, Srgba, DepthTest};
use three_d::renderer::{CpuMaterial, FragmentAttributes, Light, Material, MaterialType, Texture2DRef};


///
/// A single channel material storing rasterized font glyphs.
///
#[derive(Clone)]
pub struct TextMaterial {
    pub(crate) edge_color: Srgba,
    pub(crate) texture: Texture2DRef,
    pub(crate) render_states: RenderStates,
}

impl TextMaterial {
    pub(crate) fn new(context: &Context, cpu_material: &CpuMaterial) -> Option<Self> {
        let cpu_texture = cpu_material.albedo_texture.as_ref()?;
        Some(Self {
            edge_color: Srgba::new(0, 0, 0, 0),
            texture: Texture2DRef::from_texture(Texture2D::new(context, cpu_texture)),
            render_states: RenderStates {
                write_mask: WriteMask::COLOR,
                blend: Blend::TRANSPARENCY,
                depth_test: DepthTest::LessOrEqual,
                ..Default::default()
            },
        })
    }

    pub(crate) fn set_edge_color(&mut self, color: Option<Srgba>) {
        self.edge_color = color.unwrap_or(Srgba::new(0, 0, 0, 0));
    }

    /// Returns the GPU texture storing the currently rasterized font glyphs.
    pub fn texture(&self) -> &Texture2DRef {
        &self.texture
    }
}

impl Material for TextMaterial {
    fn id(&self) -> u16 {
        0b1u16 << 12 | 1u16 << 4
    }

    fn fragment_shader_source(&self, _lights: &[&dyn Light]) -> String {
        let mut source = String::new();
        source.push_str(ToneMapping::fragment_shader_source());
        source.push_str(ColorMapping::fragment_shader_source());
        source.push_str(include_str!("material.frag"));
        source
    }

    fn fragment_attributes(&self) -> FragmentAttributes {
        FragmentAttributes {
            color: true,
            uv: true,
            normal: true,
            ..FragmentAttributes::NONE
        }
    }

    fn use_uniforms(&self, program: &Program, camera: &Camera, _lights: &[&dyn Light]) {
        camera.tone_mapping.use_uniforms(program);
        camera.color_mapping.use_uniforms(program);
        program.use_texture("tex", &self.texture);
        program.use_uniform("edgeColor", self.edge_color.to_linear_srgb());
    }

    fn render_states(&self) -> RenderStates {
        self.render_states
    }

    fn material_type(&self) -> MaterialType {
        MaterialType::Transparent
    }
}

