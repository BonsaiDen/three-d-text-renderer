// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::error::Error;
use std::collections::HashMap;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use fontdue::{
    Font, FontSettings,
    layout::{CoordinateSystem, Layout}
};
use three_d::{
    Context, Gm,
    Interpolation, Mat4, SquareMatrix,
    Viewport, Vec2, vec2, Srgba
};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::TextRef;
use crate::cache::GlyphCache;
use crate::geometry::material::TextMaterial;
use crate::geometry::mesh::{TextMesh, TextMeshData};
use crate::glyph::PositionedGlyph;


// Types ----------------------------------------------------------------------
// ----------------------------------------------------------------------------
type TextLayoutResult<'a> = (u32, &'a TextRef<'a>, (Vec<PositionedGlyph>, Vec2));


///
/// Controls the behaviour of a [``TextBuilder``] and its corresponding [``GlyphCache``]s.
///
pub struct TextBuilderSettings {
    /// Default font scale to use - see [``fontdue::FontSettings``] for more details
    pub default_font_scale: f32,
    /// Default line height scaling to render with
    pub default_line_height: f32,
    /// Minimum size a glyph cache's texture starts and is allocated with
    pub cache_atlas_min_size: u32,
    /// Maximum size a glyph cache's texture before no further glyphs are added
    pub cache_atlas_max_size: u32,
    /// Number of pixels to be inserted on all sides of a glyph during rasterization;
    /// used to prevent UV aliasing artifacts.
    pub cache_atlas_glyph_padding: u32,
    /// Function to modify the pixel alpha values of all glyphs
    pub glyph_alpha_transform: Option<&'static dyn Fn(u8) -> u8>,
    /// Texture interpolation used by a glyph cache's texture
    pub texture_filter: Interpolation
}

impl Default for TextBuilderSettings {
    fn default() -> Self {
        Self {
            default_font_scale: 32.0,
            default_line_height: 1.0,
            cache_atlas_min_size: 64,
            cache_atlas_max_size: 4096,
            cache_atlas_glyph_padding: 2,
            glyph_alpha_transform: None,
            texture_filter: Interpolation::Linear,
        }
    }
}


///
/// Builder for creating models from a slice of [``TextRef``]'s.
///
pub struct TextBuilder {
    font: Font,
    settings: TextBuilderSettings,
    glyph_cache: HashMap<u32, GlyphCache>,
    glyph_layout: Layout,
    edge_color: Option<Srgba>,
    line_height: f32,
    viewport: Vec2,
    projection_view: Mat4
}

impl TextBuilder {
    // Scale used for calculation of line heights
    const LINE_HEIGHT_FONT_SCALE: f32 = 128.0;

    ///
    /// Create a new builder from the given TrueType Font bytes and settings.
    ///
    /// # Errors
    ///
    /// See [``fontdue::Font::from_bytes``] for possible error return values.
    ///
    pub fn new(font_bytes: &[u8], mut settings: TextBuilderSettings) -> Result<Self, Box<dyn Error>> {
        let font = Font::from_bytes(font_bytes, FontSettings {
            scale: settings.default_font_scale,
            ..Default::default()
        })?;

        // Fontdue has issues with certain fonts when rendering below the default scale
        // this can cause lines starting to overlap, to avoid this we compute an additional
        // scaling factor here and apply it tothe default line height
        let mut glyph_layout = Layout::new(CoordinateSystem::PositiveYUp);
        let s = Self::compute_line_height(&font, &mut glyph_layout, "|#g^8");
        let d = Self::compute_line_height(&font, &mut glyph_layout, "|#g^8\n|#g^8");
        let t = Self::compute_line_height(&font, &mut glyph_layout, "|#g^8\n|#g^8\n|#g^8");
        let q = Self::compute_line_height(&font, &mut glyph_layout, "|#g^8\n|#g^8\n|#g^8\n|#g^8");

        let line_height_factor = ((d / s) * 0.5 + (t / s) * 0.3333 + (q / s) * 0.25) * 0.3333;
        settings.default_line_height *= 1.0 / line_height_factor;

        Ok(Self {
            font,
            settings,
            glyph_cache: HashMap::with_capacity(8),
            glyph_layout,
            edge_color: None,
            line_height: d - s,
            viewport: vec2(0.0, 0.0),
            projection_view: Mat4::identity()
        })
    }

    /// Gets the underlying glyph cache for the given font size.
    pub fn get_glyph_cache(&mut self, font_render_size: f32) -> Option<&mut GlyphCache> {
        self.glyph_cache.get_mut(&GlyphCache::index(font_render_size))
    }

    /// Sets the viewport used by [``crate::TextAlign::Viewport``]
    /// and [``crate::TextAlign::Scene``].
    pub fn set_viewport(&mut self, viewport: Viewport) {
        self.viewport = vec2(viewport.width as f32, viewport.height as f32);
    }

    /// Sets the projection matrix used by [``crate::TextAlign::Scene``].
    pub fn set_projection_view(&mut self, matrix: Mat4) {
        self.projection_view = matrix;
    }

    /// Sets the color used for rendering debug edges of glyphs quads and text meshes.
    ///
    /// This is mostly useful for debugging purposes e.g. verifying text and
    /// character alignments.
    pub fn set_edge_color(&mut self, color: Option<Srgba>) {
        self.edge_color = color;
    }

    ///
    /// Clears all underlying glyph caches and their corresponding CPU textures.
    ///
    /// > **Note:** GPU materials of any models created via
    /// > [[``TextBuilder::build``]] will **not** be invalided by this.
    ///
    pub fn clear_caches(&mut self) {
        self.glyph_cache.clear();
    }

    ///
    /// Computes the pixel size of a given [``TextRef``].
    ///
    pub fn compute_pixel_size(&mut self, text: &TextRef) -> Vec2 {
        Self::compute_pixel_size_inner(
            text,
            &self.font,
            &mut self.glyph_layout,
            self.settings.default_line_height
        )
    }

    ///
    /// Computes the line height for the given font size.
    ///
    pub fn line_height(&mut self, size: f32) -> f32 {
        self.line_height * (size / Self::LINE_HEIGHT_FONT_SCALE)
    }

    ///
    /// Builds one ore more text models the a slice of [``TextRef``]'s.
    ///
    pub fn build<'a>(
        &mut self,
        context: &'a Context,
        texts: &[TextRef]

    ) -> impl Iterator<Item=Gm<TextMesh, TextMaterial>> + 'a {
        // Layout all glyphs and render them into the atlas texture of the corresponding caches
        let text_layout_results: Vec<TextLayoutResult> = texts.iter().filter(|l| !l.text.is_empty()).map(|text| {
            let cache_index = GlyphCache::index(text.glyph_raster_size());
            let cache = self.glyph_cache.entry(cache_index).or_insert_with(|| {
                GlyphCache::new(cache_index, &self.settings)
            });
            (
                cache_index,
                text,
                cache.layout_glyphs(
                    &self.font,
                    &mut self.glyph_layout,
                    self.settings.default_line_height,
                    text
                )
            )

        }).collect();

        // Once the caches have all the required glyphs, update their atlases and materials.
        //
        // Then create the meshes with the material UVs which are now guranteed
        // be and stay stable
        text_layout_results.into_iter().fold(
            HashMap::with_capacity(texts.len().min(4)),
            |mut meshes, (cache_index, text, (positioned_glyphs, dimensions))| {

            let cache = self.glyph_cache
                .get_mut(&cache_index)
                .expect("cache must be created before mesh creation");

            // In case the atlas has changed, a new GPU texture will get uploaded.
            // Leaving the old one untouched and thus keeping any previously
            // generated models valid
            let mut material = cache.update_material(context);
            material.set_edge_color(self.edge_color);

            // Merge all texts sharing the same font size into a single mesh
            let index_count = positioned_glyphs.len() * 6;
            let vertex_count = positioned_glyphs.len() * 4;
            let mesh_data = meshes
                .entry(cache_index)
                .or_insert_with(|| TextMeshData {
                    material,
                    indices: Vec::new(),
                    positions: Vec::new(),
                    colors: Vec::new(),
                    quad_uvs: Vec::new(),
                    glyph_uvs: Vec::new()
                });

            // Extend the mesh with the new glyphs
            mesh_data.indices.reserve(index_count);
            mesh_data.positions.reserve(vertex_count);
            mesh_data.colors.reserve(vertex_count);
            mesh_data.quad_uvs.reserve(vertex_count);
            mesh_data.glyph_uvs.reserve(vertex_count);

            // Compute offsets - for scene alignments these might be none if
            // they would lie outside the viewport
            if let Some((
                global_offset,
                local_offset

            )) = text.align.into_global_and_local_offset(
                self.projection_view,
                self.viewport,
                dimensions
            ) {
                // Build optional debug mesh to show full text bounds
                if self.edge_color.is_some() {
                    text.extend_mesh(
                        mesh_data,
                        self.viewport,
                        global_offset,
                        local_offset,
                        dimensions
                    );
                }

                // Build individual glyph meshes
                for p in positioned_glyphs {
                    p.extend_mesh(
                        mesh_data,
                        cache,
                        text,
                        self.viewport,
                        global_offset,
                        local_offset
                    );
                }
            }
            meshes

        // Return an iterator over the combined meshes
        }).into_values().filter(|data| !data.indices.is_empty()).map(|data| Gm::new(
            TextMesh::new(context, &data),
            data.material
        ))
    }
}

impl TextBuilder {
    fn compute_pixel_size_inner(text: &TextRef, font: &Font, layout: &mut Layout, line_height: f32) -> Vec2 {
        text.walk_glyphs::<(), _>(
            font,
            layout,
            line_height,
            |_, _, _, _, _| None,
        ).1
    }

    fn compute_line_height(font: &Font, layout: &mut Layout, text: &str) -> f32 {
        Self::compute_pixel_size_inner(&TextRef {
            text,
            size: Self::LINE_HEIGHT_FONT_SCALE,
            ..Default::default()

        }, font, layout, 1.0).y
    }
}

