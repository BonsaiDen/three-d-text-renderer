uniform sampler2D tex;
uniform vec4 edgeColor;

in vec2 quad_uvs;
in vec2 glyph_uvs;
in vec4 glyph_color;

layout (location = 0) out vec4 outColor;

void main() {
    // Vertex color is used for per character color
    outColor.rgb = glyph_color.rgb;

    // Texture R channel is used for glyph shape and vertex color alpha for
    // character opacity
    outColor.a = texture(tex, glyph_uvs).r * glyph_color.a;

    // Render quad edges for debugging purposes
    vec2 s = fwidth(quad_uvs);
    float edgeX = 1.0 - step(s.x, quad_uvs.x) + step(1.0 - s.x, quad_uvs.x);
    float edgeY = 1.0 - step(s.y, quad_uvs.y) + step(1.0 - s.y, quad_uvs.y);
    outColor = mix(outColor, edgeColor, max(edgeX, edgeY) * step(1.0, edgeColor.a));

    // Final color mapping
    outColor.rgb = tone_mapping(outColor.rgb);
    outColor.rgb = color_mapping(outColor.rgb);
}

