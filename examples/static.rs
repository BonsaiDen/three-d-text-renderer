use three_d::*;
use three_d_asset::io::load;
use three_d_text_builder::{TextAlign, TextBuilder, TextBuilderSettings, TextRef, TextMaterial, TextMesh};

pub fn main() {
    let window = Window::new(WindowSettings {
        title: "Text Rendering".to_string(),
        max_size: Some((1280, 720)),
        ..Default::default()
    }).unwrap();
    let context = window.gl();

    // Load our desired ttf font
    let assets = load(&["examples/ubuntu.mono.ttf"]).unwrap();

    // Create a text builder for the given font with default settings
    let mut text_builder = TextBuilder::new(
        assets.get("ubuntu.mono.ttf").unwrap(),
        TextBuilderSettings::default()

    ).expect("Failed to create text builder from TTF font");

    // Create a "2d" camera for our text to be rendered with
    let mut text_camera = Camera::new_orthographic(
        Viewport::new_at_origo(1, 1),
        vec3(0.0, 0.0, 0.0),
        vec3(0.0, 0.0, -1.0),
        vec3(0.0, 1.0, 0.0),
        0.0,
        0.1,
        10.0,
    );

    // Keep track of the viewport size so we can cheaply update our static text
    // whenever it changes
    let mut old_viewport_size = vec2(0, 0);
    let mut static_text_models: Option<Vec<Gm<TextMesh, TextMaterial>>> = None;

    window.render_loop(move |mut frame_input| {
        // Ensure the viewport size is a multiple of two (to prevent character
        // mis-alignment caused by the camera projection) and adjust it to be
        // a 1:1 pixel match with the window
        frame_input.viewport.width &= !0b1;
        frame_input.viewport.height &= !0b1;
        text_camera.set_viewport(frame_input.viewport);
        text_camera.set_orthographic_projection(frame_input.viewport.height as f32, 0.0, 1.0);

        // Set the text builder's viewport so that TextAlign::Viewport work correctly
        text_builder.set_viewport(frame_input.viewport);

        // Build fresh static text meshes everytime the viewport size changes
        let viewport_size = vec2(frame_input.viewport.width, frame_input.viewport.height);
        if static_text_models.is_none() || viewport_size != old_viewport_size {
            static_text_models = Some(text_builder.build(&context, &[
                TextRef {
                    text: "Viewport\nTop Left",
                    color: Srgba::new(255, 255, 0, 255),
                    align: TextAlign::Viewport(-1, 1),
                    ..Default::default()
                },
                TextRef {
                    text: "Viewport\nBottom Left",
                    // Use a specific size
                    size: 24.0,
                    align: TextAlign::Viewport(-1, -1),
                    // Use padding to push the text away from the viewport edges
                    padding: vec2(16.0, 16.0),
                    ..Default::default()
                },
                TextRef {
                    text: "Viewport\nTop Right",
                    size: 48.0,
                    // Use a custom line height multiplier
                    line_height: Some(1.2),
                    align: TextAlign::Viewport(1, 1),
                    ..Default::default()
                }
            ]).collect());
            old_viewport_size = viewport_size;
        }

        // Render text
        frame_input.screen()
            .clear(ClearState::color_and_depth(0.1, 0.2, 0.3, 1.0, 1.0))
            .render(&text_camera, static_text_models.as_ref().unwrap(), &[]);

        FrameOutput::default()
    });
}


