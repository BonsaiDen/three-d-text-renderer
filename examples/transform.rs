use three_d::*;
use three_d_asset::io::load;
use three_d_text_builder::{TextRef, TextBuilder, TextBuilderSettings, TextAlign};

pub fn main() {
    let window = Window::new(WindowSettings {
        title: "Text Rendering".to_string(),
        max_size: Some((1280, 720)),
        ..Default::default()
    }).unwrap();
    let context = window.gl();

    // Load our desired ttf font
    let assets = load(&["examples/ubuntu.mono.ttf"]).unwrap();

    // Create a text builder for the given font with default settings
    let mut text_builder = TextBuilder::new(
        assets.get("ubuntu.mono.ttf").unwrap(),
        TextBuilderSettings::default()

    ).expect("Failed to create text builder from TTF font");

    // Create a "2d" camera for our text to be rendered with
    let mut text_camera = Camera::new_orthographic(
        Viewport::new_at_origo(1, 1),
        vec3(0.0, 0.0, 0.0),
        vec3(0.0, 0.0, -1.0),
        vec3(0.0, 1.0, 0.0),
        0.0,
        0.1,
        10.0,
    );

    window.render_loop(move |mut frame_input| {
        // Ensure the viewport size is a multiple of two (to prevent character
        // mis-alignment caused by the camera projection) and adjust it to be
        // a 1:1 pixel match with the window
        frame_input.viewport.width &= !0b1;
        frame_input.viewport.height &= !0b1;
        text_camera.set_viewport(frame_input.viewport);
        text_camera.set_orthographic_projection(frame_input.viewport.height as f32, 0.0, 1.0);

        // Set the text builder's viewport so that TextAlign::Viewport work correctly
        text_builder.set_viewport(frame_input.viewport);

        // Render text
        let text_model = text_builder.build(&context, &[
            TextRef {
                text: "Transforming Text",
                size: 32.0,
                align: TextAlign::ScreenRelative(0, 0),
                // Different color for each character in the next
                character_color: Some(&|i, count, _| {
                    // Distribute hue across all characters
                    let hue = 1.0 / count as f32 * i as f32;
                    // Shift hue over time
                    let hue = ((frame_input.accumulated_time as f32 * 0.0001) + hue) % 1.0;
                    Srgba::from(hsv_to_rgb(hue, 1.0, 1.0))
                }),
                // Rotate the entire text around its center
                transform: Mat4::from_angle_z(degrees((-frame_input.accumulated_time * 0.125) as f32)),
                // Setup a transformation of the individual characters
                character_transform: Some(&|i, _, _| {
                    let s = (frame_input.accumulated_time * 0.001).sin() as f32 * 0.5 + 1.0;
                    let t = (frame_input.accumulated_time as f32 * 0.005 + i as f32).sin() * 20.0;
                    (
                        // Transform their positions to move in-out and up-down
                        Mat4::from_translation(vec3(0.0, t, 0.0)) * Mat4::from_nonuniform_scale(s, 1.0, 1.0),
                        // Transform their vertices so they rotate around each characters center
                        Mat4::from_angle_z(degrees((frame_input.accumulated_time * 0.25) as f32))
                    )
                }),
                ..Default::default()
            }
        ]);
        frame_input.screen()
            .clear(ClearState::color_and_depth(0.1, 0.2, 0.3, 1.0, 1.0))
            .render(&text_camera, text_model, &[]);

        FrameOutput::default()
    });
}

fn hsv_to_rgb(h: f32, s: f32, v: f32) -> [f32; 3] {
    let a = v * s;
    let b = a * (1.0 - ((h * 6.0) % 2.0 - 1.0).abs());
    let h = (h * 360.0) as u32;
    let (r, g, b) = match h {
        n if n < 60 => (a, b, 0.0),
        n if (60..120).contains(&n) => (b, a, 0.0),
        n if (120..180).contains(&n) => (0.0, a, b),
        n if (180..240).contains(&n) => (0.0, b, a),
        n if (240..300).contains(&n) => (b, 0.0, a),
        n if (300..360).contains(&n) => (a, 0.0, b),
        _ => (0.0, 0.0, 0.0)
    };
    let o = v - a;
    [r + o, g + o, b + o]
}

