// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use three_d::{Srgba, Mat4, vec2, Vec2, SquareMatrix};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{TextAlign, TextPosition, TextRef, TextWrapMode};


// Types Dependencies ---------------------------------------------------------
// ----------------------------------------------------------------------------
type CharacterColor = dyn Fn(usize, usize, char) -> Srgba;
type CharacterTransform = dyn Fn(usize, usize, char) -> (Mat4, Mat4);

///
/// Description of how an owned String should be rendered as a text mesh.
///
pub struct Text {
    /// Text to render
    pub text: String,
    /// Font size used by the resulting mesh
    pub size: f32,
    /// Font size used for glyph rasterization; when `None`, [``Text::size``] will be used
    pub raster_size: Option<f32>,
    /// Line height scaling used by the resulting mesh
    pub line_height: Option<f32>,
    /// Padding used by the resuling mesh
    pub padding: Vec2,
    /// Font color used by the resulting mesh
    pub color: Srgba,
    /// Shadow color and offset user by the resulting mesh
    pub shadow: Option<(Srgba, Vec2)>,
    /// Function to generate per-character colors
    pub character_color: Option<Box<CharacterColor>>,
    /// Alignment used by the resulting mesh.
    pub align: TextAlign,
    /// Maximum width of the text before wrapping will be applied
    pub max_width: Option<f32>,
    /// Wrapping mod to apply when `Text::max_width` is exceeded
    pub wrap_mode: TextWrapMode,
    /// Position used by the resulting; see [``TextAlign``] for how this is computed
    pub position: TextPosition,
    /// Transform used by the resulting mesh
    pub transform: Mat4,
    /// Function to generate per-character position and vertex transforms
    pub character_transform: Option<Box<CharacterTransform>>
}

impl Default for Text {
    fn default() -> Self {
        Self {
            text: "Text".to_string(),
            size: 32.0,
            raster_size: None,
            line_height: None,
            padding: vec2(0.0, 0.0),
            color: Srgba::WHITE,
            character_color: None,
            align: TextAlign::default(),
            max_width: None,
            wrap_mode: TextWrapMode::default(),
            shadow: None,
            position: TextPosition::Pixels(vec2(0.0, 0.0)),
            transform: Mat4::identity(),
            character_transform: None
        }
    }
}

impl Text {
    /// Creates a new text description for the given String.
    pub fn new<S: Into<String>>(text: S) -> Self {
        Self {
            text: text.into(),
            ..Default::default()
        }
    }

    /// Specifies the font size used by the resulting mesh.
    pub fn size(mut self, size: f32) -> Self {
        self.size = size;
        self
    }

    /// Specifies the font size used for glyph rasterization; when `None`, [``Text::size``] will be used.
    pub fn raster_size(mut self, raster_size: f32) -> Self {
        self.raster_size = Some(raster_size);
        self
    }

    /// Specifies the line height scaling used by the resulting mesh.
    pub fn line_height(mut self, line_height: f32) -> Self {
        self.line_height = Some(line_height);
        self
    }

    /// Specifies the padding used by the resulting mesh.
    pub fn padding(mut self, padding: Vec2) -> Self {
        self.padding = padding;
        self
    }

    /// Specifies the font color used by the resulting mesh.
    pub fn color(mut self, color: Srgba) -> Self {
        self.color = color;
        self
    }

    /// Specifies the shadow color and offset user by the resulting mesh.
    pub fn shadow(mut self, color: Srgba, offset: Vec2) -> Self {
        self.shadow = Some((color, offset));
        self
    }

    /// Specifies the function to generate per-character colors.
    ///
    /// These colors do overide the color set by [``Text::color``].
    pub fn character_color(mut self, color: Box<CharacterColor>) -> Self {
        self.character_color = Some(color);
        self
    }

    /// Specifies the alignment used by the resulting mesh.
    pub fn align(mut self, align: TextAlign) -> Self {
        self.align = align;
        self
    }

    /// Specifies the position used by the resulting; see [``TextAlign``] for how this is computed.
    pub fn position(mut self, position: TextPosition) -> Self {
        self.position = position;
        self
    }

    /// Specifies the transform used by the resulting mesh.
    pub fn transform(mut self, transform: Mat4) -> Self {
        self.transform = transform;
        self
    }

    /// Specifies the function to generate per-character position and vertex transforms.
    pub fn character_transform(mut self, transform: Box<CharacterTransform>) -> Self {
        self.character_transform = Some(transform);
        self
    }

    /// Creates a [``TextRef``] referencing this text's data.
    pub fn as_ref(&self) -> TextRef {
        TextRef {
            text: self.text.as_str(),
            size: self.size,
            raster_size: self.raster_size,
            line_height: self.line_height,
            padding: self.padding,
            color: self.color,
            character_color: self.character_color.as_deref(),
            align: self.align,
            max_width: self.max_width,
            wrap_mode: self.wrap_mode,
            shadow: self.shadow,
            position: self.position,
            transform: self.transform,
            character_transform: self.character_transform.as_deref(),
        }
    }
}

