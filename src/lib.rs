#![warn(clippy::all)]
#![warn(missing_docs)]
#![warn(unsafe_code)]
//!
//! Glyph atlas based text rendering for the [three-d](https://crates.io/crates/three-d)
//! crate, using [fontdue](https://crates.io/crates/fontdue).
//!
//! This crate's aim is to provide decent performance and quality of the rendered text.
//!
//! It is suited for both large static text elements as well as dynamically
//! formatted text that quickly changes.
//!
//! # Quick Example
//!
//! ```rust
//! // Create a text builder from a TTF font
//! let mut text_builder = TextBuilder::new(
//!     &include_str!("font.ttf"),
//!     TextBuilderSettings::default()
//!
//! ).unwrap();
//!
//! // Create some text
//! let text = TextRef {
//!     // The text to render
//!     text: "The quick brown fox jumps over the lazy dog",
//!     // Set the color
//!     color: Srgba::RED,
//!     // Align to the lower center edge of the viewport
//!     align: TextAlign::Viewport(0, -1),
//!     // Add some padding
//!     padding: vec2(0.0, 8.0),
//!     // Move up by 25% of the viewport's height
//!     position: TextPosition::Percentage(vec2(0.0, 0.25)),
//!     // Add a simple shadow effect
//!     shadow: Some((Srgba::BLACK, vec2(1.0, -1.0))),
//!     ..Default::default()
//! };
//!
//! // Build a 3D model for our text - this could also be cached for later usage
//! // to avoid building the same model multiple times
//! let text_model = text_builder.build(&context, &[text]);
//!
//! // Setup the viewport so our alignment works as expected
//! text_builder.set_viewport(viewport);
//!
//! // Then render it to a target
//! render_target.render(&camera, text_model, &[]);
//! ```
//!
//! For more use cases, check out the standalone examples.
//!
//!
//! # Implementation Notes
//!
//! Each [``TextBuilder``] uses a set of [``GlyphCache``]s (one for each font size)
//! which produce materials from their glyph atlases. These get mapped to
//! the UVs of generated character quads, which in turn get combined into a
//! set of meshes. Text meshes that share the same underlying atlas material
//! get combined into a single mesh in order to improve drawing performance.
//!
//! In addition, a copy on write approach is employed to keep the number of
//! uploaded GPU materials to a minimum, while also allow generated models to
//! be cached by the application code.
//!

// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::cmp::Ordering;


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use fontdue::layout::WrapStyle;
use three_d::{Mat4, vec2, Vec2, Vec3, Transform};


// Modules --------------------------------------------------------------------
// ----------------------------------------------------------------------------
mod builder;
mod cache;
mod geometry;
mod glyph;
mod text;
mod text_ref;


// Re-Exports -----------------------------------------------------------------
// ----------------------------------------------------------------------------
pub use builder::{TextBuilder, TextBuilderSettings};
pub use cache::GlyphCache;
pub use geometry::material::TextMaterial;
pub use geometry::mesh::TextMesh;
pub use text::Text;
pub use text_ref::TextRef;


///
/// Specifies the unit used by the `position` property of a [``TextRef``].
///
#[derive(Debug, Clone, Copy)]
pub enum TextPosition {
    ///
    /// Position is interpreted as pixel values.
    ///
    /// # Examples
    ///
    /// - `TextPosition::Pixels(vec2(0.0, 64.0))`: Move the text up by exactly `64` pixels
    ///
    Pixels(Vec2),
    ///
    /// Position is interpreted as a percentage of the viewport's size.
    ///
    /// # Examples
    ///
    /// - `TextPosition::Percentage(vec2(0.5, 0.0))`: Move the text to the right by half the viewport's width in pixels
    ///
    Percentage(Vec2)
}

impl TextPosition {
    fn to_pixels(self, viewport: Vec2) -> Vec2 {
        match self {
            Self::Pixels(p) => p,
            Self::Percentage(p) => vec2(viewport.x * p.x, viewport.y * p.y)
        }
    }
}

///
/// Specifies how the `position` property of a [``TextRef``] is applied.
///
#[derive(Debug, Clone, Copy)]
pub enum TextAlign {
    ///
    /// Positions the text relative to the bottom left corner of the screen,
    /// then anchors it with respect to its bounding box.
    ///
    /// # Examples
    ///
    /// - `TextAlign::Screen(0, 0)`: Anchors at the center
    /// - `TextAlign::Screen(1, -1)`: Anchors at the upper left corner
    ///
    ScreenAbsolute(i32, i32),

    ///
    /// Positions the text relative to the center of the screen,
    /// then anchors it with respect to its bounding box.
    ///
    /// # Examples
    ///
    /// - `TextAlign::Screen(0, 0)`: Anchors at the center
    /// - `TextAlign::Screen(1, -1)`: Anchors at the upper left corner
    ///
    ScreenRelative(i32, i32),

    ///
    /// Anchors the text to the edges of the viewport (as set by
    /// [``TextBuilder::set_viewport``]),
    /// then positions it with respect to its bounding box.
    ///
    /// # Examples
    ///
    /// - `TextAlign::Viewport(-1, -1)`: Anchors at the lower left corner of the viewport
    /// - `TextAlign::Viewport(1, 0)`: Anchors at the right center edge of the viewport
    ///
    Viewport(i32, i32),

    ///
    /// Positions the text relative to the edges of the viewport (as set by
    /// [``TextBuilder::set_viewport``]),
    /// then positions it with respect to its bounding box.
    ///
    /// # Examples
    ///
    /// - `TextAlign::ViewportRelative(-1, -1, 1, 1)`: Anchors at the lower left corner of the viewport (identical to to `TextAlign::Viewport(-1, -1)`)
    /// - `TextAlign::ViewportRelative(-1, -1, 0, 0)`: Anchors at the lower left corner of the viewport, centering the text on the given position
    ///
    ViewportRelative(i32, i32, i32, i32),

    ///
    /// Anchors the text to the projected position of the 3D point (as described by
    /// [``TextBuilder::set_projection_view``]),
    /// then positions it with respect to its bounding box.
    ///
    /// # Examples
    ///
    /// - `TextAlign::Scene(vec(0.0, 0.0, 0.0), 0, 0)`: Projects the scene's origin then anchors at the center
    ///
    Scene(Vec3, i32, i32),
}

impl Default for TextAlign {
    fn default() -> Self {
        Self::ScreenRelative(0, 0)
    }
}

impl TextAlign {
    /// Calculates the global and local offets required to align a rectangle with
    /// the given bounds inside the specified viewport.
    ///
    /// > **Important:** This assumes that the rectangle's lower left corner is
    /// > at the coordinates `0, 0`.
    pub fn into_global_and_local_offset(
        self,
        world_to_screen: Mat4,
        viewport: Vec2,
        bounds: Vec2

    ) -> Option<(Vec2, Vec2)> {
        fn align_bounds(axis: i32, value: f32) -> f32 {
            match axis.cmp(&0) {
                // CEIL here so right-aligned text is stable in its position
                Ordering::Equal => -(value * 0.5).ceil(),
                Ordering::Greater => 0.0,
                Ordering::Less => -value
            }
        }

        fn align_viewport(axis: i32, value: f32) -> f32 {
            // FLOOR here so right-aligned text is stable in its position
            (axis as f32 * value * 0.5).floor()
        }

        fn edges(axis: i32, value: f32) -> (f32, f32) {
            match axis.cmp(&0) {
                Ordering::Equal => ((value * 0.5).ceil(), -(value * 0.5).floor()),
                Ordering::Greater => (value, 0.0),
                Ordering::Less => (0.0, -value)
            }
        }

        match self {
            Self::ScreenAbsolute(x, y) => Some((
                // Offset from the bottom left of the viewport
                vec2(align_bounds(0, viewport.x), align_bounds(0, viewport.y)),
                // Center on the given position
                vec2(align_bounds(x, bounds.x), -align_bounds(-y, bounds.y))
            )),
            Self::ScreenRelative(x, y) => Some((
                // Offset from the center of the viewport
                vec2(0.0, 0.0),
                // Center on the given position
                vec2(align_bounds(x, bounds.x), -align_bounds(-y, bounds.y))
            )),
            Self::Viewport(x, y) => Some((
                // Offset from the edges of the viewport
                vec2(
                    align_viewport(x, viewport.x) - align_viewport(x, bounds.x),
                    align_viewport(y, viewport.y) - align_viewport(y, bounds.y)
                ),
                // Center on the given position
                vec2(align_bounds(0, bounds.x), -align_bounds(0, bounds.y))
            )),
            Self::ViewportRelative(x, y, lx, ly) => Some((
                // Offset from the edges of the viewport
                vec2(
                    align_viewport(x, viewport.x),
                    align_viewport(y, viewport.y)
                ),
                // Center on the given position
                vec2(align_bounds(lx, bounds.x), -align_bounds(-ly, bounds.y))
            )),
            Self::Scene(p, x, y) => {
                // Project the scene point onto the screen
                let p = world_to_screen.transform_point(three_d::Point3::new(p.x, p.y, p.z));

                // Check if in front of the camera
                if p.z < 1.0 {
                    let center = vec2(align_bounds(0, viewport.x), align_bounds(0, viewport.y));
                    let sx = p.x * 0.5 + 0.5;
                    let sy = p.y * 0.5 + 0.5;

                    // Calculate position on screen
                    let global_offset = vec2(sx * viewport.x, sy * viewport.y);
                    let local_offset = vec2(align_bounds(x, bounds.x), -align_bounds(-y, bounds.y));

                    // Clip bounds against viewport
                    let (t, b) = edges(y, bounds.y);
                    let (r, l) = edges(x, bounds.x);
                    let within_x = global_offset.x + r >= 0.0 && global_offset.x + l < viewport.x;
                    let within_y = global_offset.y + t >= 0.0 && global_offset.y + b < viewport.y;
                    if within_x && within_y {
                        Some((
                            // Offset from the bottom left of the viewport
                            center + global_offset,
                            // Center on the given position
                            local_offset
                        ))

                    } else {
                        None
                    }

                } else {
                    None
                }
            }
        }
    }
}

///
/// Specifies how the text is wrapped when the maximum width is exceeded.
///
#[derive(Debug, Default, Copy, Clone, PartialEq, Eq, Hash)]
pub enum TextWrapMode {
    /// Word based wrapping, see [`fontdue::layout::WrapStyle::Word`] for details
    #[default]
    Word,
    /// Letter based wrapping, see [`fontdue::layout::WrapStyle::Letter`] for details
    Letter
}

impl From<TextWrapMode> for WrapStyle {
    fn from(value: TextWrapMode) -> Self {
        match value {
            TextWrapMode::Word => Self::Word,
            TextWrapMode::Letter => Self::Letter
        }
    }
}

