use three_d::*;
use three_d_asset::io::load;
use three_d_text_builder::{TextRef, TextBuilder, TextBuilderSettings, TextAlign};

pub fn main() {
    let window = Window::new(WindowSettings {
        title: "Text Rendering".to_string(),
        max_size: Some((1280, 720)),
        ..Default::default()
    }).unwrap();
    let context = window.gl();

    // Load our desired ttf font
    let assets = load(&["examples/ubuntu.mono.ttf"]).unwrap();

    // Create a text builder for the given font with default settings
    let mut text_builder = TextBuilder::new(
        assets.get("ubuntu.mono.ttf").unwrap(),
        TextBuilderSettings::default()

    ).expect("Failed to create text builder from TTF font");

    // Create a "2d" camera for our text to be rendered with
    let mut text_camera = Camera::new_orthographic(
        Viewport::new_at_origo(1, 1),
        vec3(0.0, 0.0, 0.0),
        vec3(0.0, 0.0, -1.0),
        vec3(0.0, 1.0, 0.0),
        0.0,
        0.1,
        10.0,
    );

    // Setup a simple 3D scene
    let mut scene_camera = Camera::new_perspective(
        window.viewport(),
        vec3(5.0, 2.0, 2.5),
        vec3(0.0, 0.0, -0.5),
        vec3(0.0, 1.0, 0.0),
        degrees(45.0),
        0.1,
        1000.0,
    );
    let mut control = OrbitControl::new(*scene_camera.target(), 1.0, 100.0);
    let mut cube = Gm::new(
        Mesh::new(&context, &CpuMesh::cube()),
        PhysicalMaterial::new_opaque(
            &context,
            &CpuMaterial {
                albedo: Srgba::new_opaque(128, 128, 128),
                ..Default::default()
            },
        ),
    );
    let light = DirectionalLight::new(&context, 1.0, Srgba::WHITE, &vec3(0.0, -0.5, -0.5));
    let cube_position = vec3(0.0, 0.0, 1.3);
    cube.set_transformation(Mat4::from_translation(cube_position) * Mat4::from_scale(0.2));

    window.render_loop(move |mut frame_input| {
        // Render the 3D scene
        scene_camera.set_viewport(frame_input.viewport);
        control.handle_events(&mut scene_camera, &mut frame_input.events);
        frame_input.screen()
            .clear(ClearState::color_and_depth(0.1, 0.2, 0.3, 1.0, 1.0))
            .render(&scene_camera, &cube, &[&light]);

        // Ensure the viewport size is a multiple of two (to prevent character
        // mis-alignment caused by the camera projection) and adjust it to be
        // a 1:1 pixel match with the window
        frame_input.viewport.width &= !0b1;
        frame_input.viewport.height &= !0b1;
        text_camera.set_viewport(frame_input.viewport);
        text_camera.set_orthographic_projection(frame_input.viewport.height as f32, 0.0, 1.0);

        // Set the text builder's viewport so that TextAlign::Viewport work correctly
        text_builder.set_viewport(frame_input.viewport);

        // Important: Setup the projection * view matrix so the text builder can
        // compute TextAlign::Scene(...) alignment correctly
        text_builder.set_projection_view(scene_camera.projection() * scene_camera.view());

        // Render text
        let text_model = text_builder.build(&context, &[
            // Place the text above our cube in the 3D
            TextRef {
                text: "Cube",
                size: 24.0,
                // Center onto the cube's screen position
                align: TextAlign::Scene(cube_position, 0, 0),
                ..Default::default()
            }
        ]);
        frame_input.screen()
            .render(&text_camera, text_model, &[]);

        FrameOutput::default()
    });
}


