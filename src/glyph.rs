// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use fontdue::Metrics;
use rectangle_pack::PackedLocation;
use three_d::{vec2, vec3, vec4, Vec2, Vec4, Mat4, Srgba, Transform};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::TextRef;
use crate::cache::GlyphCache;
use crate::geometry::mesh::TextMeshData;


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub struct PositionedGlyph {
    character: char,
    position: Vec2,
    transform: Option<(Mat4, Mat4)>,
    color: Option<Srgba>,
    height: f32,
    width: f32
}

impl PositionedGlyph {
    pub fn new(
        character: char,
        position: Vec2,
        width: f32,
        height: f32,
        transform: Option<(Mat4, Mat4)>,
        color: Option<Srgba>

    ) -> Self {
        Self {
            character,
            position,
            transform,
            color,
            width,
            height
        }
    }

    pub(crate) fn extend_mesh(
        &self,
        mesh_data: &mut TextMeshData,
        cache: &GlyphCache,
        text: &TextRef,
        viewport: Vec2,
        global_offset: Vec2,
        local_offset: Vec2
    ) {
        let glyph = cache.get_glyph(self.character).expect("positioned glyph must have been cached");
        let global_offset = text.position.to_pixels(viewport) + global_offset;

        // Shadow mesh - offset is applied in local character space
        if let Some((color, offset)) = text.shadow {
            self.extend_mesh_with_color(
                mesh_data,
                glyph,
                text,
                global_offset,
                local_offset + offset,
                color.to_linear_srgb()
            );
        }

        // Normal mesh
        self.extend_mesh_with_color(
            mesh_data,
            glyph,
            text,
            global_offset,
            local_offset,
            // Individual character color does take precedence
            self.color.unwrap_or(text.color).to_linear_srgb()
        );
    }

    fn extend_mesh_with_color(
        &self,
        mesh_data: &mut TextMeshData,
        glyph: &CachedGlyph,
        text: &TextRef,
        global_offset: Vec2,
        local_offset: Vec2,
        color: Vec4
    )  {
        // Indices
        let index = mesh_data.positions.len() as u32;
        mesh_data.indices.extend_from_slice(&[index, index + 1, index + 2, index + 2, index + 3, index]);

        // Colors
        mesh_data.colors.extend_from_slice(&[color, color, color, color]);

        // Edge coordinates
        mesh_data.quad_uvs.extend_from_slice(&[
            vec2(1.0, 1.0),
            vec2(1.0, 0.0),
            vec2(0.0, 0.0),
            vec2(0.0, 1.0)
        ]);

        // UVs
        mesh_data.glyph_uvs.extend_from_slice(&[
            vec2(glyph.uvs.z, 1.0 - glyph.uvs.y),
            vec2(glyph.uvs.z, 1.0 - glyph.uvs.w),
            vec2(glyph.uvs.x, 1.0 - glyph.uvs.w),
            vec2(glyph.uvs.x, 1.0 - glyph.uvs.y)
        ]);

        // Calculate the local center of the glyph
        let position = self.position + local_offset + vec2(text.padding.x, -text.padding.y);
        let offset = vec3(self.width * 0.5, self.height * 0.5, 0.0);
        let mut center = offset + vec3(position.x.round(), position.y.round(), 0.0);

        // Offset quad vertices so the are locally transformed around their center
        let mut tl = -offset + vec3(0.0, 0.0, 0.0);
        let mut br = -offset + vec3(self.width, self.height, 0.0);
        let mut bl = -offset + vec3(0.0, self.height, 0.0);
        let mut tr = -offset + vec3(self.width, 0.0, 0.0);

        // Apply local glyph transforms
        if let Some((origin_transform, vertex_transform)) = &self.transform {
            center = origin_transform.transform_vector(center) + origin_transform.w.truncate();
            br = vertex_transform.transform_vector(br);
            tr = vertex_transform.transform_vector(tr);
            tl = vertex_transform.transform_vector(tl);
            bl = vertex_transform.transform_vector(bl);
        }

        // Apply global text transform and translation
        let translation = vec3(
            (global_offset.x + text.transform.w.x).round(),
            (global_offset.y + text.transform.w.y).round(),
            text.transform.w.z.round()
        );
        mesh_data.positions.extend_from_slice(&[
            text.transform.transform_vector(center + br) + translation,
            text.transform.transform_vector(center + tr) + translation,
            text.transform.transform_vector(center + tl) + translation,
            text.transform.transform_vector(center + bl) + translation
        ]);
    }
}

pub struct CachedGlyph {
    metrics: Metrics,
    offset: Vec2,
    pixels: Vec<u8>,
    atlas_position: Option<(u32, u32)>,
    uvs: Vec4
}

impl CachedGlyph {
    pub fn new(metrics: Metrics, offset: Vec2, pixels: Vec<u8>) -> Self {
        Self {
            metrics,
            offset,
            pixels,
            atlas_position: None,
            uvs: vec4(0.0, 0.0, 0.0, 0.0)
        }
    }

    pub fn metrics(&self) -> &Metrics {
        &self.metrics
    }

    pub fn offset(&self) -> Vec2 {
        self.offset
    }

    pub fn invalidate(&mut self) {
        self.atlas_position = None;
    }

    pub fn render_to_texture(
        &mut self,
        texture_data: &mut [u8],
        texture_width: u32,
        location: &PackedLocation,
        padding: u32,
        glyph_alpha_transform: Option<&'static dyn Fn(u8) -> u8>

    ) -> usize {
        let p = padding as usize;
        let width = self.metrics.width;
        let height = self.metrics.height;

        // Calculate new position in the atlas
        let tx = location.x();
        let ty = location.y();
        let position = Some((tx, ty));

        // Only render glyphs whose position changed
        if position != self.atlas_position  {
            self.atlas_position = position;

            let tx = tx as usize;
            let ty = ty as usize;

            // Render pixels with additional padding to avoid UV aliasing artifacts
            for y in 0..height + p * 2 {
                for x in 0..width + p * 2 {
                    let texture_index = (ty + y) * texture_width as usize + (tx + x);
                    let alpha = if y >= p && y < height + p && x >= p && x < width + p {
                        let glyph_index = (y - p) * width + (x - p);
                        self.pixels[glyph_index]

                    } else {
                        0
                    };
                    texture_data[texture_index] = if let Some(transform) = glyph_alpha_transform {
                        transform(alpha)

                    } else {
                        alpha
                    };
                }
            }

            // Update glyph UVs
            let tx = tx + p;
            let ty = ty + p;
            let unit = 1.0 / texture_width as f32;
            self.uvs = vec4(
                unit * tx as f32,
                unit * ty as f32,
                unit * (tx + width) as f32,
                unit * (ty + height) as f32
            );
        }

        // Return number of pixels used
        (width + p * 2) * (height + p * 2)
    }
}

