// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use three_d::{Srgba, Mat4, vec2, Vec2, vec3, vec4, SquareMatrix, Transform};
use fontdue::{
    Font,
    layout::{GlyphPosition, Layout, LayoutSettings, TextStyle}
};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::{TextAlign, TextPosition, TextWrapMode};
use crate::geometry::mesh::TextMeshData;


///
/// Description of how a string slice should be rendered as a text mesh.
///
pub struct TextRef<'a> {
    /// Text to render
    pub text: &'a str,
    /// Font size used by the resulting mesh
    pub size: f32,
    /// Font size used for glyph rasterization; when `None`, [``TextRef::size``] will be used
    pub raster_size: Option<f32>,
    /// Line height scaling used by the resulting mesh
    pub line_height: Option<f32>,
    /// Padding used by the resuling mesh
    pub padding: Vec2,
    /// Font color used by the resulting mesh
    pub color: Srgba,
    /// Shadow color and offset user by the resulting mesh
    pub shadow: Option<(Srgba, Vec2)>,
    /// Function to generate per-character colors
    pub character_color: Option<&'a dyn Fn(usize, usize, char) -> Srgba>,
    /// Alignment used by the resulting mesh.
    pub align: TextAlign,
    /// Maximum width of the text before wrapping will be applied
    pub max_width: Option<f32>,
    /// Wrapping mod to apply when `Text::max_width` is exceeded
    pub wrap_mode: TextWrapMode,
    /// Position used by the resulting; see [``TextAlign``] for how this is computed
    pub position: TextPosition,
    /// Transform used by the resulting mesh
    pub transform: Mat4,
    /// Function to generate per-character position and vertex transforms
    pub character_transform: Option<&'a dyn Fn(usize, usize, char) -> (Mat4, Mat4)>
}

impl<'a> Default for TextRef<'a> {
    fn default() -> Self {
        Self {
            text: "Text",
            size: 32.0,
            raster_size: None,
            line_height: None,
            padding: vec2(0.0, 0.0),
            color: Srgba::WHITE,
            character_color: None,
            align: TextAlign::default(),
            max_width: None,
            wrap_mode: TextWrapMode::default(),
            shadow: None,
            position: TextPosition::Pixels(vec2(0.0, 0.0)),
            transform: Mat4::identity(),
            character_transform: None
        }
    }
}

impl<'a> TextRef<'a> {
    /// Creates a new text description for the given string slice.
    pub fn new(text: &'a str) -> Self {
        Self {
            text,
            ..Default::default()
        }
    }

    /// Specifies the font size used by the resulting mesh.
    pub fn size(mut self, size: f32) -> Self {
        self.size = size;
        self
    }

    /// Specifies the font size used for glyph rasterization; when `None`, [``TextRef::size``] will be used.
    pub fn raster_size(mut self, raster_size: f32) -> Self {
        self.raster_size = Some(raster_size);
        self
    }

    /// Specifies the line height scaling used by the resulting mesh.
    pub fn line_height(mut self, line_height: f32) -> Self {
        self.line_height = Some(line_height);
        self
    }

    /// Specifies the padding used by the resulting mesh.
    pub fn padding(mut self, padding: Vec2) -> Self {
        self.padding = padding;
        self
    }

    /// Specifies the font color used by the resulting mesh.
    pub fn color(mut self, color: Srgba) -> Self {
        self.color = color;
        self
    }

    /// Specifies the shadow color and offset user by the resulting mesh.
    pub fn shadow(mut self, color: Srgba, offset: Vec2) -> Self {
        self.shadow = Some((color, offset));
        self
    }

    /// Specifies the function to generate per-character colors.
    ///
    /// These colors do overide the color set by [``TextRef::color``].
    pub fn character_color(mut self, color: &'a dyn Fn(usize, usize, char) -> Srgba) -> Self {
        self.character_color = Some(color);
        self
    }

    /// Specifies the alignment used by the resulting mesh.
    pub fn align(mut self, align: TextAlign) -> Self {
        self.align = align;
        self
    }

    /// Specifies the position used by the resulting; see [``TextAlign``] for how this is computed.
    pub fn position(mut self, position: TextPosition) -> Self {
        self.position = position;
        self
    }

    /// Specifies the transform used by the resulting mesh.
    pub fn transform(mut self, transform: Mat4) -> Self {
        self.transform = transform;
        self
    }

    /// Specifies the function to generate per-character position and vertex transforms
    pub fn character_transform(mut self, transform: &'a dyn Fn(usize, usize, char) -> (Mat4, Mat4)) -> Self {
        self.character_transform = Some(transform);
        self
    }
}

impl<'a> TextRef<'a> {
    pub(crate) fn glyph_raster_size(&self) -> f32 {
        self.raster_size.unwrap_or(self.size)
    }

    pub(crate) fn walk_glyphs<
        T,
        C: FnMut(
            usize,
            usize,
            &GlyphPosition,
            Option<&'a dyn Fn(usize, usize, char) -> (Mat4, Mat4)>,
            Option<&'a dyn Fn(usize, usize, char) -> Srgba>

        ) -> Option<T>

    >(&self, font: &Font, layout: &mut Layout, line_height: f32, mut callback: C) -> (Vec<T>, Vec2) {
        layout.reset(&LayoutSettings {
            line_height: self.line_height.unwrap_or(line_height),
            max_width: self.max_width,
            wrap_style: self.wrap_mode.into(),
            ..Default::default()
        });
        layout.append(&[font], &TextStyle::new(self.text, self.size, 0));

        // Fontdue generates incosistent widths for the last character in a text.
        //
        // This can cause visible jitter when this character changes very frequently.
        //
        // To avoid this we, push another blank character onto the string, this will
        // cause fontdue to provide consistent width calculations and results
        // in stable positioning of the generated text meshes.
        if !self.text.is_empty() {
            layout.append(&[font], &TextStyle::new(" ", self.size, 0));
        }

        let count = layout.glyphs().len();
        let mut results = Vec::with_capacity(count);
        let mut bounds = vec2(0.0f32, 0.0);
        for (index, glyph) in layout.glyphs().iter().enumerate() {
            if let Some(result) = callback(index, count, glyph, self.character_transform, self.character_color) {
                results.push(result);
            }
            bounds.x = bounds.x.max(glyph.x + glyph.width as f32);
            bounds.y = bounds.y.max(glyph.y.abs());
        }
        (results, bounds + self.padding * 2.0)
    }

    pub(crate) fn extend_mesh(
        &self,
        mesh_data: &mut TextMeshData,
        viewport: Vec2,
        global_offset: Vec2,
        local_offset: Vec2,
        dimensions: Vec2
    ) {
        // Vertex Data
        let index = mesh_data.positions.len() as u32;
        mesh_data.indices.extend_from_slice(&[index, index + 1, index + 2, index + 2, index + 3, index]);

        let color = vec4(0.0, 0.0, 0.0, 0.0);
        mesh_data.colors.extend_from_slice(&[color, color, color, color]);
        mesh_data.quad_uvs.extend_from_slice(&[vec2(1.0, 1.0), vec2(1.0, 0.0), vec2(0.0, 0.0), vec2(0.0, 1.0)]);
        mesh_data.glyph_uvs.extend_from_slice(&[vec2(0.0, 0.0), vec2(0.0, 0.0), vec2(0.0, 0.0), vec2(0.0, 0.0)]);

        // Build vertices so they match the bounding box without the padding
        let width = dimensions.x - self.padding.x * 2.0;
        let height = dimensions.y - self.padding.y * 2.0;
        let offset = vec3(self.padding.x, -height - self.padding.y, 0.0);

        // Compute quad vertices
        let tl = offset + vec3(0.0, 0.0, 0.0);
        let br = offset + vec3(width, height, 0.0);
        let bl = offset + vec3(0.0, height, 0.0);
        let tr = offset + vec3(width, 0.0, 0.0);

        // Calculate the center of the text
        let center = vec3(local_offset.x.round(), local_offset.y.round(), 0.0);

        // Apply global text transform and translation
        let g = self.position.to_pixels(viewport) + global_offset;
        let translation = vec3(g.x.round(), g.y.round(), 0.0);
        mesh_data.positions.extend_from_slice(&[
            self.transform.transform_vector(center + br) + translation,
            self.transform.transform_vector(center + tr) + translation,
            self.transform.transform_vector(center + tl) + translation,
            self.transform.transform_vector(center + bl) + translation
        ]);
    }
}

