// STD Dependencies -----------------------------------------------------------
// ----------------------------------------------------------------------------
use std::collections::{BTreeMap, HashMap};


// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use fontdue::{
    Font,
    layout::Layout
};
use three_d::{
    vec2,
    Vec2,
    Srgba, Context,
    CpuTexture, CpuMaterial,
    TextureData, Wrapping
};
use rectangle_pack::{
    GroupedRectsToPlace, RectToInsert, TargetBin, RectanglePackOk
};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::TextRef;
use crate::builder::TextBuilderSettings;
use crate::geometry::material::TextMaterial;
use crate::glyph::{CachedGlyph, PositionedGlyph};


///
/// Rasterizes and caches glyphs for a specific font / size combination.
///
/// # Implementation
///
/// Glyphs get rasterized on demand when first encountered and are packed into
/// a CPU backed atlas texture, when the atlas can no longer fit any additional
/// glyphs its size doubles until it reaches the maximum allowed size.
///
/// Once the maximum atlas size has been reached, no further glyphs will get
/// rasterized any any characters that require them will be skipped when building
/// the meshes.
///
/// When an atlas gets updated with new glyphs, its GPU texture is uploaded to
/// the GPU as a new material for use with the text meshes.
///
/// > **Note:** Font sizes are rounded to the first decimal place.
///
/// > **Note:** Glyphs get only rasterized once, but may get copied multiple times
/// > into the backing CPU texture in case their position in the atlas changes.
///
pub struct GlyphCache {
    raster_size: f32,

    atlas_is_exhausted: bool,
    atlas_requires_update: bool,
    atlas_fill_ratio: f32,
    atlas_max_size: u32,
    atlas_rects: GroupedRectsToPlace<char>,

    glyph_padding: u32,
    glyph_alpha_transform: Option<&'static dyn Fn(u8) -> u8>,
    glyphs: HashMap<char, CachedGlyph>,

    cpu_material: CpuMaterial,
    gpu_material: Option<TextMaterial>
}

impl GlyphCache {
    /// Returns `true` in case there are currently no glyphs in the cache
    pub fn is_empty(&self) -> bool {
        self.glyphs.is_empty()
    }

    /// Returnss the number of rasterized glyphs in the cache
    pub fn len(&self) -> usize {
        self.glyphs.len()
    }

    /// Returns the size of the CPU texture in use by the cache
    pub fn size(&self) -> (u32, u32) {
        let texture = self.cpu_material.albedo_texture.as_ref().expect("cpu material must always be present");
        (texture.width, texture.height)
    }

    /// Returns how much of the CPU texture's area is currently used by glyphs
    pub fn fill_ratio(&self) -> f32 {
        self.atlas_fill_ratio
    }

    ///
    /// Returns `true` when no further glyphs can be fit into the cache and the
    /// maximum texture size as allowed by [``TextBuilderSettings``] has
    /// already been reached
    ///
    pub fn is_exhausted(&self) -> bool {
        self.atlas_is_exhausted
    }

    ///
    /// Returns a reference to the cache's currently uploaded GPU material
    ///
    pub fn material(&self) -> Option<&TextMaterial> {
        self.gpu_material.as_ref()
    }

    pub(crate) fn index(size: f32) -> u32 {
        (size * 10.0) as u32
    }

    pub(crate) fn new(size: u32, settings: &TextBuilderSettings) -> Self {
        let u = settings.cache_atlas_min_size;
        let bytes = (u * u) as usize;
        Self {
            raster_size: size as f32 * 0.1,

            atlas_is_exhausted: false,
            atlas_requires_update: true,
            atlas_fill_ratio: 0.0,
            atlas_max_size: settings.cache_atlas_max_size,
            atlas_rects: GroupedRectsToPlace::new(),

            glyph_padding: settings.cache_atlas_glyph_padding,
            glyph_alpha_transform: settings.glyph_alpha_transform,
            glyphs: HashMap::with_capacity(32),

            cpu_material: CpuMaterial {
                albedo: Srgba::from([1.0, 1.0, 1.0, 1.0]),
                albedo_texture: Some(CpuTexture {
                    name: format!("text_renderer_cache_{size}"),
                    data: TextureData::RU8(std::iter::repeat(0).take(bytes).collect()),
                    width: u,
                    height: u,
                    min_filter: settings.texture_filter,
                    mag_filter: settings.texture_filter,
                    mip_map_filter: None,
                    wrap_s: Wrapping::ClampToEdge,
                    wrap_t: Wrapping::ClampToEdge
                }),
                ..Default::default()
            },
            gpu_material: None
        }
    }

    pub(crate) fn get_glyph(&self, character: char) -> Option<&CachedGlyph> {
        self.glyphs.get(&character)
    }

    pub(crate) fn layout_glyphs(
        &mut self,
        font: &Font,
        layout: &mut Layout,
        line_height: f32,
        text: &TextRef

    ) -> (Vec<PositionedGlyph>, Vec2) {
        text.walk_glyphs(font, layout, line_height, |index, count, glyph, transform, color| {
            // We only rasterize unknown glyphs when the atlas has not exhausted its maximum size
            let is_known_glyph = self.glyphs.contains_key(&glyph.parent);

            // Skip any whitespace...
            if glyph.parent.is_whitespace() ||
                // ...and don't create positions for unknown glyphs when exhausted
                self.atlas_is_exhausted && !is_known_glyph {
                None

            // Only rasterize glyphs we don't have cached in the atlas yet
            } else if !is_known_glyph {
                // Support glyph fallbacks
                let ((metrics, pixels), offset) = if !font.has_glyph(glyph.parent) {
                    let (metrics, pixels) = font.rasterize(Self::fallback_glyph(glyph.parent), self.raster_size);
                    ((metrics, pixels), vec2(0.0, metrics.ymin as f32))

                } else {
                    (font.rasterize(glyph.parent, self.raster_size), vec2(0.0, 0.0))
                };
                self.atlas_rects.push_rect(
                    glyph.parent,
                    None,
                    RectToInsert::new(
                        metrics.width as u32 + self.glyph_padding * 2,
                        metrics.height as u32 + self.glyph_padding * 2,
                        1
                    )
                );
                self.glyphs.insert(glyph.parent, CachedGlyph::new(metrics, offset, pixels));
                self.atlas_requires_update = true;
                Some(PositionedGlyph::new(
                    glyph.parent,
                    vec2(glyph.x, glyph.y) + offset,
                    metrics.width as f32,
                    metrics.height as f32,
                    transform.map(|t| t(index, count, glyph.parent)),
                    color.map(|t| t(index, count, glyph.parent))
                ))

            // Return any glyphs that we have already cached
            } else if let Some(cached) = self.glyphs.get(&glyph.parent) {
                let metrics = cached.metrics();
                Some(PositionedGlyph::new(
                    glyph.parent,
                    vec2(glyph.x, glyph.y) + cached.offset(),
                    metrics.width as f32,
                    metrics.height as f32,
                    transform.map(|t| t(index, count, glyph.parent)),
                    color.map(|t| t(index, count, glyph.parent))
                ))
            } else {
                None
            }
        })
    }

    pub(crate) fn update_material(&mut self, context: &Context) -> TextMaterial {
        // Re-pack our atlas texture if any new glyphs have been added
        if self.atlas_requires_update {
            self.atlas_requires_update = false;
            match self.update_atlas() {
                Some(()) => {
                    self.gpu_material = None;
                },
                None => {
                    self.atlas_is_exhausted = true;
                }
            }
        }

        // Upload a new material to the GPU if required
        if self.gpu_material.is_none() {
            self.gpu_material = TextMaterial::new(context, &self.cpu_material);
        }

        // Return a clone of the material for binding to models
        self.gpu_material.as_ref().unwrap().clone()
    }

    fn update_atlas(&mut self) -> Option<()> {
        // Try to repack all glyphs into the atlas,
        // increasing its size if both necessary and possible
        let texture = self.cpu_material.albedo_texture.as_ref().unwrap();
        let (packing, required_size) = self.repack_atlas(texture.width)?;

        // Update Texture
        let texture = self.cpu_material.albedo_texture.as_mut().unwrap();
        if let TextureData::RU8(ref mut texture_data) = texture.data {
            // Resize texture if required (by extending with the required number of bytes)
            if texture.width != required_size {
                let current = (texture.width * texture.height) as usize;
                let target = (required_size * required_size) as usize;
                let missing = target - current;
                texture_data.extend_from_slice(&vec![0; missing]);
                texture.width = required_size;
                texture.height = required_size;

                // Invalidate all glyphs positions
                for glyph in self.glyphs.values_mut() {
                    glyph.invalidate();
                }
            }

            // Write glyph alpha data into the texture
            let mut filled_pixels = 0;
            for (character, (_, location)) in packing.packed_locations() {
                if let Some(glyph) = self.glyphs.get_mut(character) {
                    filled_pixels += glyph.render_to_texture(
                        texture_data,
                        texture.width,
                        location,
                        self.glyph_padding,
                        self.glyph_alpha_transform
                    );
                }
            }
            let available_pixels = texture.width * texture.height;
            self.atlas_fill_ratio = if available_pixels > 0 {
                1.0 / available_pixels as f32 * filled_pixels as f32

            } else {
                1.0
            };
        }
        Some(())
    }

    fn repack_atlas(&mut self, mut current_size: u32) -> Option<(RectanglePackOk<char, i32>, u32)> {
        loop {
            let mut target_bins = BTreeMap::new();
            target_bins.insert(0, TargetBin::new(current_size, current_size, 1));

            // Try to pack the glyphs into the available atlas size
            if let Ok(result) = rectangle_pack::pack_rects(
                &self.atlas_rects,
                &mut target_bins,
                &rectangle_pack::volume_heuristic,
                &rectangle_pack::contains_smallest_box
            ) {
                return Some((result, current_size));

            // Re-try with another size
            } else if current_size < self.atlas_max_size {
                current_size *= 2;

            // If we have already reached the maximum size but still cannot fit
            // all the glyphs, we consider ourselves exhausted and will no
            // longer accept / render any unknown glyphs that we encounter
            } else {
                return None;
            }
        };
    }

    fn fallback_glyph(character: char) -> char {
        match character {
            // Non-breaking-hypen
            '\u{2011}' => '-',
            c => c
        }
    }

}

