# three-d-text-builder

Glyph atlas based text rendering for the [three-d](https://crates.io/crates/three-d) 
crate, using [fontdue](https://crates.io/crates/fontdue).

> **Important:** This crate is *not* associated with the `three-d` project itself 
> and the project will not provide any support.

## Features

- Fast and cheap text mesh generation for dynamic text elements
- Efficient static text mesh generation for fixed text elements 
- Easy alignment of text to the viewport or 3D scene
- API to build multiple text meshes at once and auto-merge them if possible
    - Supports individual font size, color, alignment and transform for each text mesh
    - Supports indididual color and transform for each text character in a mesh

See the examples for a demonstration of the supported features.

## License

Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)
at your option.

