// External Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use three_d::{
    AxisAlignedBoundingBox, Camera, Context,
    Geometry, Material,
    ColorTexture, DepthTexture,
    FragmentAttributes, Effect, Light,
    Program, RenderStates,
    Vec2, Vec3, Vec4, Mat4, SquareMatrix, ElementBuffer, VertexBuffer,
    renderer::{render_with_material, render_with_effect}
};


// Internal Dependencies ------------------------------------------------------
// ----------------------------------------------------------------------------
use crate::geometry::material::TextMaterial;


// Structs --------------------------------------------------------------------
// ----------------------------------------------------------------------------
pub(crate) struct TextMeshData {
    pub(crate) material: TextMaterial,
    pub(crate) indices: Vec<u32>,
    pub(crate) positions: Vec<Vec3>,
    pub(crate) colors: Vec<Vec4>,
    pub(crate) quad_uvs: Vec<Vec2>,
    pub(crate) glyph_uvs: Vec<Vec2>
}


///
/// A simple mesh geometry rendering mulitple rasterized font glyphs.
///
pub struct TextMesh {
    aabb: AxisAlignedBoundingBox,
    context: Context,

    indices: ElementBuffer,
    positions: VertexBuffer,
    colors: VertexBuffer,
    quad_uvs: VertexBuffer,
    glyph_uvs: VertexBuffer,
}

impl TextMesh {
    pub(crate) fn new(context: &Context, data: &TextMeshData) -> Self {
        let mut aabb = AxisAlignedBoundingBox::EMPTY;
        aabb.expand(&data.positions);

        Self {
            aabb,
            context: context.clone(),
            indices: ElementBuffer::new_with_data(context, &data.indices),
            positions: VertexBuffer::new_with_data(context, &data.positions),
            colors: VertexBuffer::new_with_data(context, &data.colors),
            quad_uvs: VertexBuffer::new_with_data(context, &data.quad_uvs),
            glyph_uvs: VertexBuffer::new_with_data(context, &data.glyph_uvs)
        }
    }
}

impl Geometry for TextMesh {
    fn aabb(&self) -> AxisAlignedBoundingBox {
        self.aabb
    }

    fn id(&self, _required_attributes: FragmentAttributes) -> u16 {
        0b1u16 << 12 | 1u16 << 4
    }

    fn draw(
        &self,
        camera: &Camera,
        program: &Program,
        render_states: RenderStates,
        _: FragmentAttributes,
    ) {
        if self.positions.count() > 0 {
            program.use_vertex_attribute("position", &self.positions);
            program.use_vertex_attribute("color", &self.colors);
            program.use_vertex_attribute("quad_coordinates", &self.quad_uvs);
            program.use_vertex_attribute("glyph_coordinates", &self.glyph_uvs);
            program.use_uniform("viewProjection", camera.projection() * camera.view());
            program.use_uniform("modelMatrix", Mat4::identity());
            program.draw_elements(render_states, camera.viewport(), &self.indices);
        }
    }

    fn vertex_shader_source(&self, _: FragmentAttributes) -> String {
        include_str!("mesh.vert").to_string()
    }

    fn render_with_material(
        &self,
        material: &dyn Material,
        camera: &Camera,
        lights: &[&dyn Light],
    ) {
        render_with_material(&self.context, camera, &self, material, lights);
    }

    fn render_with_effect(
        &self,
        material: &dyn Effect,
        camera: &Camera,
        lights: &[&dyn Light],
        color_texture: Option<ColorTexture>,
        depth_texture: Option<DepthTexture>,
    ) {
        render_with_effect(
            &self.context,
            camera,
            self,
            material,
            lights,
            color_texture,
            depth_texture,
        );
    }
}

